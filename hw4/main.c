#include <stdio.h>
#include <stdlib.h>

#define N1 10
#define N2 10
#define N  20

void printpoly(int, const int []);

int* multpoly(int dest[],
              int n1, const int poly1[],
              int n2, const int poly2[]);

int main()
{
    int poly1[N1];
    int poly2[N2];
    int poly[N] = {}; // 모두 0으로 초기화

    for (int i=0; i<N1; ++i) scanf("%d", &poly1[i]); // poly1 입력 받기
    for (int i=0; i<N2; ++i) scanf("%d", poly2 + i); // poly2 입력 받기
    multpoly(poly, N1, poly1, N2, poly2); // poly에 poly1과 poly2의 곱을 저장

    printf("  "); printpoly(N1, poly1); printf("\n");
    printf("* "); printpoly(N2, poly2); printf("\n");
    printf("------------------------------------------------------------\n");
    printf("  "); printpoly(N, poly); printf("\n");

    return 0;
}


void printpoly(int n, const int poly[])
{
  for(int i=0; i<n; ++i)
   {
   if(poly[i]>0)
   {
       if(i==0)
       {
       printf(" %d",poly[i]);
       }
       else if(i==1)
       {
           printf(" +%dx",poly[i]);
       }
       else if(i>1)
       {
           if(poly[i]==1)
           {
           printf(" +x^%d",i);
           }
           else
            printf(" +%dx^%d",poly[i],i);
       }
   }
   else if(poly[i]<0)
   {
     if(i==0)
       {
       printf(" %d",poly[i]);
       }
     else if(i==1)
       {
           if(poly[i]==-1)
           {
           printf(" -x");
           }
           else
           {
           printf(" %dx",poly[i]);
           }
       }
     else if(i>1)
       {
           if(poly[i]==-1)
           {
           printf(" -x^%d",i);
           }
           else
             printf(" %dx^%d",poly[i],i);
       }
   }

   else if (poly[i] == 0)
   {
   }
   }
}
int* multpoly(int dest[],
              int n1, const int poly1[],
              int n2, const int poly2[])
{
    // 아래는 poly에 poly1을 그대로 복사하는 더미 구현이므로 곱셈을 하도록 수정 (1점)
    for(int i=0; i<n1; ++i) dest[i] = poly1[i];

    return dest;
}
